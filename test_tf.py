import crow.src as crow
import tensorflow as tf

input_size = 784
hidden_size = 256
output_size = 10

input_holder = tf.placeholder(tf.float32, [None, input_size], name="input_holder")
label_holder = tf.placeholder(tf.float32, [None, output_size], name="label_holder")

stddev = 0.01

w1 = crow.normal_variable([input_size, hidden_size], stddev)
b1 = crow.normal_variable([hidden_size], stddev)
alpha1 = crow.zeros_variable([hidden_size])

w2 = crow.normal_variable([hidden_size, hidden_size], stddev)
b2 = crow.normal_variable([hidden_size], stddev)
alpha2 = crow.zeros_variable([hidden_size])

w3 = crow.normal_variable([hidden_size, output_size], stddev)
b3 = crow.normal_variable([output_size], stddev)
alpha3 = crow.zeros_variable([output_size])

matmul1 = tf.matmul(input_holder, w1) + b1
prelu1 = crow.prelu(matmul1, alpha1)
matmul2 = tf.matmul(prelu1, w2) + b2
prelu2 = crow.prelu(matmul2, alpha2)
matmul3 = tf.matmul(prelu2, w3) + b3
prelu3 = crow.prelu(matmul3, alpha3)
output = tf.nn.softmax(prelu3, name="output")

loss = crow.cross_entropy(output, label_holder)
optimizer = crow.AdaBoundOptimizer().minimize(loss, name="optimizer")
tf.global_variables_initializer()
saver_def = tf.train.Saver().as_saver_def()

with open("test_graph.pb", "wb") as file_obj:
    file_obj.write(tf.get_default_graph().as_graph_def().SerializeToString())
