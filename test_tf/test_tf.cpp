﻿#include "pch.h"
#include <vector>
#include <iostream>
#include <cassert>
#include <string>
#include <fstream>
#include <random>
#include "tensorflow/core/framework/tensor.h"
#include "tensorflow/core/public/session.h"

namespace tf = tensorflow;


tf::Tensor vectorToTensor(const std::vector<std::vector<float>>& data) {
	int batch_size = (int)data.size();
	int ele_size = (int)data[0].size();

	tf::Tensor result(tf::DT_FLOAT,
		              tf::TensorShape({batch_size, ele_size}));
	int start = 0;
	for (int i = 0; i < batch_size; i++) {
		for (int j = 0; j < ele_size; j++) {
			result.flat<float>()(j + start) = data[i][j];
		}
		start += ele_size;
	}
	return result;
}

std::vector<std::string> readStrMnistOneData(const std::string file_path) {
	std::ifstream ifs(file_path);
	assert(!(ifs.fail()));

	std::string str;
	std::vector<std::string> result;

	while (getline(ifs, str)) {
		result.emplace_back(str);
	}
	return result;
}

std::vector<float> readMnistOneData(const std::string file_path) {
	std::vector<std::string> str_data = readStrMnistOneData(file_path);
	std::vector<float> result;

	for (int i = 0; i < str_data.size(); i++) {
		result.emplace_back(std::stod(str_data[i]));
	}
	return result;
}

std::vector<std::vector<float>> readMnistBatchData(std::string folder_path, int size) {
	std::vector<std::vector<float>> result;
	for (int i = 0; i < size; i++) {
		std::string file_path = folder_path + std::to_string(i) + ".txt";
		result.emplace_back(readMnistOneData(file_path));
	}
	return result;
}

std::vector<int> miniBatchIndicesRandomChoice(int range, int size, std::mt19937 mt) {
	std::vector<int> result;
	std::uniform_int_distribution<int> randint(0, range - 1);
	for (int i = 0; i < size; i++) {
		result.emplace_back(randint(mt));
	}
	return result;
}

std::vector<std::vector<float>> miniBatchRandomChoice(std::vector<std::vector<float>> data, std::vector<int> indices) {
	std::vector<std::vector<float>> result;
	for (int i = 0; i < indices.size(); i++) {
		result.emplace_back(data[indices[i]]);
	}
	return result;
}

bool isCorrect(const std::vector<std::vector<float>>& input_data, const std::vector<float>& label_data, const std::unique_ptr<tf::Session>& session) {
	std::vector<tf::Tensor> output;
	TF_CHECK_OK(session -> Run({ {"input_holder", vectorToTensor(input_data)} },
		                       { "output" }, {}, &output));

	
	float max_value = output[0].flat<float>()(0);
	int max_value_index = 0;
	for (int i = 0; i < label_data.size(); i++) {
		float value = output[0].flat<float>()(i);
		if (value > max_value) {
			max_value = value;
			max_value_index = i;
		}
	}

	int correct_index = 0;
	for (int i = 0; label_data.size(); i++) {
		if (label_data[i] == 1) {
			correct_index = i;
			break;
		}
	}
	return max_value_index == correct_index;
}

float accuracy(const std::vector<std::vector<float>>& input_data, const std::vector<std::vector<float>>& label_data, const std::unique_ptr<tf::Session>& session) {
	int count = 0;
	for (int i = 0; i < input_data.size(); i++) {
		std::vector<std::vector<float>> tmp;
		tmp.emplace_back(input_data[i]);
		if (isCorrect(tmp, label_data[i], session)) {
			count++;
		}
	}
	return count / (float)input_data.size();
}

int main()
{
	const std::string graph_def_file_name = "test_graph.pb";
	const std::string mnist_folder_path = "txt_mnist/";
	const std::string input_data_folder_path =  mnist_folder_path + "input_data/";
	const std::string label_data_folder_path = mnist_folder_path + "label_data/";
	const std::string test_data_folder_path = mnist_folder_path + "test_data/";
	const std::string test_label_data_folder_path = mnist_folder_path + "test_label_data/";

	int batch_size = 10000;
	int test_batch_size = 1000;

	std::cout << "input_dataの読み込み開始" << std::endl;
	std::vector<std::vector<float>> input_data = readMnistBatchData(input_data_folder_path, batch_size);
	std::cout << "input_dataの読み込み終了" << std::endl;

	std::cout << "label_dataの読み込み開始" << std::endl;
	std::vector<std::vector<float>> label_data = readMnistBatchData(label_data_folder_path, batch_size);
	std::cout << "label_dataの読み込み終了" << std::endl;

	std::cout << "test_dataの読み込み開始" << std::endl;
	std::vector<std::vector<float>> test_data = readMnistBatchData(test_data_folder_path, test_batch_size);
	std::cout << "test_dataの読み込み終了" << std::endl;

	std::cout << "test_label_dataの読み込み開始" << std::endl;
	std::vector<std::vector<float>> test_label_data = readMnistBatchData(test_label_data_folder_path, test_batch_size);
	std::cout << "test_label_dataの読み込み終了" << std::endl;

	std::random_device rand_device;
	std::mt19937 mt;

	tf::GraphDef graph_def;
	TF_CHECK_OK(tf::ReadBinaryProto(tf::Env::Default(),
		                            graph_def_file_name, &graph_def));
	std::unique_ptr<tf::Session> session(tf::NewSession(tf::SessionOptions()));

	TF_CHECK_OK(session -> Create(graph_def));
	TF_CHECK_OK(session -> Run({}, {}, { "init" }, nullptr));
	
	int iter_num = 12800;
	for (int i = 0; i < iter_num; i++) {
		std::vector<int> indices = miniBatchIndicesRandomChoice(batch_size, 16, mt);
		std::vector<std::vector<float>> miniBatchInputData = miniBatchRandomChoice(input_data, indices);
		std::vector<std::vector<float>> miniBatchLabelData = miniBatchRandomChoice(label_data, indices);
		TF_CHECK_OK(session->Run({ {"input_holder", vectorToTensor(miniBatchInputData)},
								   {"label_holder", vectorToTensor(miniBatchLabelData)} },
			                      {}, {"optimizer"}, nullptr));
		mt.seed(rand_device());
		if (i % 100 == 0) {
			std::vector<int> indices = miniBatchIndicesRandomChoice(test_batch_size, 128, mt);
			std::vector<std::vector<float>> miniBatchTestData = miniBatchRandomChoice(test_data, indices);
			std::vector<std::vector<float>> miniBatchTestLabelData = miniBatchRandomChoice(test_label_data, indices);
			std::cout << accuracy(miniBatchTestData, miniBatchTestLabelData, session) << std::endl;
			mt.seed(rand_device());
		}
	}
	return 0;
}

// プログラムの実行: Ctrl + F5 または [デバッグ] > [デバッグなしで開始] メニュー
// プログラムのデバッグ: F5 または [デバッグ] > [デバッグの開始] メニュー

// 作業を開始するためのヒント: 
//    1. ソリューション エクスプローラー ウィンドウを使用してファイルを追加/管理します 
//   2. チーム エクスプローラー ウィンドウを使用してソース管理に接続します
//   3. 出力ウィンドウを使用して、ビルド出力とその他のメッセージを表示します
//   4. エラー一覧ウィンドウを使用してエラーを表示します
//   5. [プロジェクト] > [新しい項目の追加] と移動して新しいコード ファイルを作成するか、[プロジェクト] > [既存の項目の追加] と移動して既存のコード ファイルをプロジェクトに追加します
//   6. 後ほどこのプロジェクトを再び開く場合、[ファイル] > [開く] > [プロジェクト] と移動して .sln ファイルを選択します
